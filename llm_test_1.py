# imports
import ast  # for converting embeddings saved as strings back to arrays
from openai import OpenAI # for calling the OpenAI API
import pandas as pd  # for storing text and embeddings data
import tiktoken  # for counting tokens
# import os # for getting API token from env variable OPENAI_API_KEY
from scipy import spatial  # for calculating vector similarities for search

# models
GPT_MODEL = "gpt-3.5-turbo"
api_key = "sk-proj-qxmYdcB0RWqQSJE7CFcpT3BlbkFJRMJ28htiCSNAuI6Z4vFj"
client = OpenAI(api_key=api_key)

# def num_tokens(text: str, model: str = GPT_MODEL) -> int:
#     """Return the number of tokens in a string."""
#     encoding = tiktoken.encoding_for_model(model)
#     return len(encoding.encode(text))

def query_message(
    query: str,
    model: str,
    token_budget: int
) -> str:
    # message = prompt[min(num_tokens(query, model=model), token_budget)]
    message = query
    return message

def ask(
    query: str,
    model: str = GPT_MODEL,
    token_budget: int = 4096 - 500,
    print_message: bool = False,
) -> str:
    message = query_message(query, model=model, token_budget=token_budget)

    response = client.chat.completions.create(
        messages=[
            {'role': 'system', 'content': 'You are an assistant that only provides code solution.'},
            {'role': 'user', 'content': message},
        ],
        model=model,
        max_tokens=500,  # Adjust the max_tokens parameter based on your desired length
        temperature=0.1
    )

    response_message = response.choices[0].message.content
    if print_message:
        print(message)

    return response_message

print(ask('code python to create prime number', print_message=False))