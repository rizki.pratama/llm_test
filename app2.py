# openai vers  openai== 1.23.6

from flask import Flask, render_template, request
import openai  # Correct import statement
import ast  # for converting embeddings saved as strings back to arrays
import pandas as pd  # for storing text and embeddings data
import tiktoken  # for counting tokens
# import os # for getting API token from env variable OPENAI_API_KEY
from scipy import spatial  # for calculating vector similarities for search

app = Flask(__name__)

# Initialize OpenAI client
api_key = "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"
openai.api_key = api_key  # Set API key for openai client
GPT_MODEL = "text-davinci-003"

def ask(
    query: str,
    model: str = GPT_MODEL,
    token_budget: int = 4096 - 500,
    print_message: bool = False,
) -> str:
    message = query  # Since query_message() is not defined, we'll directly use the query

    try:
        response = openai.ChatCompletion.create(
            messages=[
                {'role': 'system', 'content': 'You are an assistant that only provides code solution.'},
                {'role': 'user', 'content': message},
            ],
            model=model,
            max_tokens=500,  # Adjust the max_tokens parameter based on your desired length
            temperature=0.1
        )

        response_message = response.choices[0].message.content
        if print_message:
            print(message)

        return response_message
    except Exception as e:
        error_message = f"An error occurred in generating code: {str(e)}"
        app.logger.error(error_message)
        return f"Error: {str(e)}"


@app.route('/')
def home():
    return render_template('index.html')

@app.route('/generate_code', methods=['POST'])
def generate_code():
    try:
        query = request.form['query']
        code = ask(query)
        return render_template('index.html', query=query, code=code)
    except Exception as e:
        error_message = f"An error occurred: {str(e)}"
        app.logger.error(error_message)
        return render_template('error.html', error_message=error_message), 500


if __name__ == '__main__':
    app.run(debug=True)
