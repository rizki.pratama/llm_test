from flask import Blueprint, render_template, redirect, url_for, flash, request
from flask_login import login_user, current_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from models import db, User, Chat
from forms import LoginForm, ChatForm
from init import login_manager  # Ensure this is correctly imported

routes_app = Blueprint('routes_app', __name__)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

@routes_app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('routes_app.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('routes_app.home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', form=form)

@routes_app.route('/')
@login_required
def home():
    form = ChatForm()
    user_chats = Chat.query.filter_by(user_id=current_user.id).all()
    return render_template('index.html', form=form, chats=user_chats)

@routes_app.route('/generate_code', methods=['POST'])
@login_required
def generate_code():
    try:
        query = request.form['prompt']
        code = ask(query)
        chat = Chat(user_id=current_user.id, prompt=query, response=code)
        db.session.add(chat)
        db.session.commit()
        return redirect(url_for('routes_app.home'))
    except Exception as e:
        error_message = f"An error occurred: {str(e)}"
        app.logger.error(error_message)
        return render_template('error.html', error_message=error_message), 500

@routes_app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('routes_app.login'))

def ask(query: str, model: str = "gpt-3.5-turbo", token_budget: int = 4096 - 500, print_message: bool = False) -> str:
    message = query
    try:
        response = openai.ChatCompletion.create(
            messages=[
                {'role': 'system', 'content': 'You are an assistant that only provides code solution.'},
                {'role': 'user', 'content': message},
            ],
            model=model,
            max_tokens=500,
            temperature=0.1
        )

        response_message = response.choices[0].message.content
        if print_message:
            print(message)

        return response_message
    except Exception as e:
        error_message = f"An error occurred in generating code: {str(e)}"
        app.logger.error(error_message)
        return f"Error: {str(e)}"
