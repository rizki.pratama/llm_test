import sqlite3
import json
from datetime import datetime

DATABASE_NAME = 'chatbot.db'

def connect():
    return sqlite3.connect(DATABASE_NAME)

def create_tables():
    conn = connect()
    cursor = conn.cursor()
    
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS Users (
        user_id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT NOT NULL UNIQUE,
        password TEXT NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
    ''')
    
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS Chats (
        chat_id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER,
        prompt TEXT,
        reply TEXT,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (user_id) REFERENCES Users(user_id)
    );
    ''')
    
    conn.commit()
    conn.close()

def insert_user(username, password):
    conn = connect()
    cursor = conn.cursor()
    
    cursor.execute('''
    INSERT INTO Users (username, password) VALUES (?, ?)
    ''', (username, password))
    
    user_id = cursor.lastrowid
    conn.commit()
    conn.close()
    
    return user_id

def insert_chat_message(user_id, prompt, reply):
    conn = connect()
    cursor = conn.cursor()
    
    cursor.execute('''
    INSERT INTO Chats (user_id, prompt, reply) VALUES (?, ?, ?)
    ''', (user_id, prompt, reply))
    
    conn.commit()
    conn.close()

def get_users():
    conn = connect()
    cursor = conn.cursor()
    
    cursor.execute('SELECT * FROM Users')
    users_data = cursor.fetchall()
    conn.close()
    
    users_json = json.dumps([{
        'user_id': row[0],
        'username': row[1],
        'password': row[2],
        'created_at': row[3]
    } for row in users_data], default=str)
    
    return users_json

def get_chats():
    conn = connect()
    cursor = conn.cursor()
    
    cursor.execute('SELECT * FROM Chats')
    chats_data = cursor.fetchall()
    conn.close()
    
    chats_json = json.dumps([{
        'chat_id': row[0],
        'user_id': row[1],
        'prompt': row[2],
        'reply': row[3],
        'created_at': row[4]
    } for row in chats_data], default=str)
    
    return chats_json