from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

# Initialize the database and login manager
db = SQLAlchemy()
login_manager = LoginManager()

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = b'\x85\xb6\xf3\x82\x97\x9c\xb7\xf1\x0e\x97\xb1\xcf\x12\xd4\xf0\xfa\x89\x1b\xea\xb3\x17\x1a\x1a\xb3'  # Use the generated secret key
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'

    db.init_app(app)

    # Initialize LoginManager
    login_manager.init_app(app)
    login_manager.login_view = 'routes_app.login'

    with app.app_context():
        from models import User, Chat  # Import models for creating tables
        db.create_all()  # Create database tables

        from routes import routes_app  # Import and register blueprints
        app.register_blueprint(routes_app)

    return app

if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)
